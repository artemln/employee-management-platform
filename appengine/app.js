const express = require('express');
const PubSub = require('@google-cloud/pubsub');

// Create a new PubSub client using the GOOGLE_CLOUD_PROJECT
// environment variable. This is automatically set to the correct
// value when running on AppEngine.
const pubsubClient = new PubSub({
  projectId: process.env.GOOGLE_CLOUD_PROJECT
});

const app = express();

app.get('/cron/birthday-email-hours-tick', async (req, res) => {
  // Run only inside AppEngine.
  if (req.get('X-Appengine-Cron') !== 'true') {
    console.error('invalid header', req.get('X-Appengine-Cron'));

    res.status(500).send('').end()
  }

  try {
    await pubsubClient.topic('birthday-email-hours-tick')
      .publisher()
      .publish(Buffer.from('tick'));

    res.status(200).send('Emails checked.').end();
  } catch (e) {
    console.error('tick error', e);

    res.status(500).send('' + e).end();
  }
});

// Start the server
const PORT = process.env.PORT || 6060;

app.listen(PORT, () => {
  console.log(`App listening on port ${PORT}`);
  console.log('Press Ctrl+C to quit.');
});
