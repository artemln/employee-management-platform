import {registerLocaleData} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {LOCALE_ID, NgModule} from '@angular/core';
import {AngularFireModule} from '@angular/fire';
import {AngularFireStorageModule} from '@angular/fire/storage';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {environment} from '../environments/environment';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {EmployeeCardComponent} from './components/employee-card/employee-card.component';
import {EmployeeCreatePageComponent} from './components/employee-create-page/employee-create-page.component';
import {EmployeeEditPageComponent} from './components/employee-edit-page/employee-edit-page.component';
import {EmployeeFormComponent} from './components/employee-form/employee-form.component';
import {EmployeeListCardComponent} from './components/employee-list-card/employee-list-card.component';
import {EmployeeListPageComponent} from './components/employee-list-page/employee-list-page.component';
import {EmployeeListComponent} from './components/employee-list/employee-list.component';
import {EmployeePageComponent} from './components/employee-page/employee-page.component';
import {NotFoundComponent} from './components/not-found/not-found.component';
import { SettingsPageComponent } from './components/settings-page/settings-page.component';
import localeRu from '@angular/common/locales/ru';
import { LayoutComponent } from './components/layout/layout.component';

registerLocaleData(localeRu);

@NgModule({
  declarations: [
    AppComponent,
    EmployeeListComponent,
    EmployeeCardComponent,
    EmployeePageComponent,
    EmployeeListPageComponent,
    EmployeeListCardComponent,
    NotFoundComponent,
    EmployeeEditPageComponent,
    EmployeeFormComponent,
    EmployeeCreatePageComponent,
    SettingsPageComponent,
    LayoutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireStorageModule
  ],
  providers: [ { provide: LOCALE_ID, useValue: 'ru' } ],
  bootstrap: [AppComponent]
})
export class AppModule { }
