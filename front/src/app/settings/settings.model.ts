
export interface IEmailSettings {
  beforeDays: number;
  sendTime: number;
}
