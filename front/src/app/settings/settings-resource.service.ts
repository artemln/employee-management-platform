import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {IEmailSettings} from './settings.model';

@Injectable({
  providedIn: 'root'
})
export class SettingsResourceService {

  private readonly endpoint = 'https://employee-management-platform.firebaseio.com/settings';

  constructor(
    private http: HttpClient
  ) {
  }

  get(): Observable<IEmailSettings> {
    const headers = new HttpHeaders({'X-Firebase-Decoding': '1'});

    return this.http.get<IEmailSettings>(`${this.endpoint}.json`, { headers });
  }

  patch(settings: IEmailSettings): Observable<IEmailSettings> {
    const headers = new HttpHeaders({'X-Firebase-Decoding': '1'});

    return this.http.patch<IEmailSettings>(`${this.endpoint}.json`, settings, { headers });
  }
}
