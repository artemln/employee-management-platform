import { TestBed } from '@angular/core/testing';

import { SettingsResourceService } from './settings-resource.service';

describe('SettingsResourceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SettingsResourceService = TestBed.get(SettingsResourceService);
    expect(service).toBeTruthy();
  });
});
