import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {EmployeeCreatePageComponent} from './components/employee-create-page/employee-create-page.component';
import {EmployeeEditPageComponent} from './components/employee-edit-page/employee-edit-page.component';
import {EmployeeListPageComponent} from './components/employee-list-page/employee-list-page.component';
import {EmployeePageComponent} from './components/employee-page/employee-page.component';
import {NotFoundComponent} from './components/not-found/not-found.component';
import {SettingsPageComponent} from './components/settings-page/settings-page.component';
import {EmployeeFormCanDeactivateGuard} from './guards/employee-form-can-deactivate.guard';
import {EmployeeResolveGuard} from './guards/employee-resolve.guard';

const routes: Routes = [
  {
    path: '',
    component: EmployeeListPageComponent
  },
  {
    path: 'employee/:id',
    resolve: {
      employee: EmployeeResolveGuard,
    },
    runGuardsAndResolvers: 'always',
    children: [
      {
        path: '',
        component: EmployeePageComponent,
      },
      {
        path: 'edit',
        canDeactivate: [
          EmployeeFormCanDeactivateGuard
        ],
        component: EmployeeEditPageComponent,
      }
    ]
  },
  {
    path: 'employee-add',
    canDeactivate: [
      EmployeeFormCanDeactivateGuard
    ],
    component: EmployeeCreatePageComponent
  },
  {
    path: 'settings',
    component: SettingsPageComponent
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
