import {ChangeDetectionStrategy, Component, HostListener, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {IEmployee} from '../../employee/employee.model';
import * as md5 from 'js-md5';

@Component({
  selector: 'em-employee-list-card',
  templateUrl: './employee-list-card.component.html',
  styleUrls: ['./employee-list-card.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EmployeeListCardComponent implements OnInit {

  @Input()
  item: IEmployee;

  constructor(
    private router: Router
  ) {
  }

  ngOnInit() {
  }

  public getAvatar() {
    return this.item.photo ? this.item.photo.url : `https://www.gravatar.com/avatar/${md5(this.item.email)}?s=120`
  }

  @HostListener('click', ['$event'])
  onCardClick(e: MouseEvent) {
    e.preventDefault();

    this.router.navigate(['/employee', this.item.id]);
  }
}
