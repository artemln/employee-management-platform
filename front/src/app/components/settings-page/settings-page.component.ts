import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {SettingsResourceService} from '../../settings/settings-resource.service';
import {IEmailSettings} from '../../settings/settings.model';

@Component({
  selector: 'em-settings-page',
  templateUrl: './settings-page.component.html',
  styleUrls: ['./settings-page.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SettingsPageComponent implements OnInit {

  public form = this.formBuilder.group({
    beforeDays: ['', [Validators.required, Validators.min(0), Validators.max(40)]],
    sendTime: ['', [Validators.required, Validators.min(0), Validators.max(23)]],
  });

  private currentSettings: IEmailSettings;

  constructor(
    private cd: ChangeDetectorRef,
    private router: Router,
    private formBuilder: FormBuilder,
    private settingsResource: SettingsResourceService
  ) {
  }

  ngOnInit() {
    this.settingsResource.get().subscribe(settings => {
      this.currentSettings = settings;

      this.form.reset(this.currentSettings);
    })
  }

  public onSubmit() {
    if (this.form.valid) {
      this.settingsResource.patch(this.form.value).subscribe(() =>
        this.router.navigate(['/'])
      )
    }
  }

  public onCancel() {
    this.form.reset(this.currentSettings);
  }
}
