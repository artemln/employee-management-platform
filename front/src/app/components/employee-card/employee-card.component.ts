import {ChangeDetectionStrategy, Component, Input} from '@angular/core';
import {IEmployee} from '../../employee/employee.model';
import * as md5 from 'js-md5';

@Component({
  selector: 'em-employee-card',
  templateUrl: './employee-card.component.html',
  styleUrls: ['./employee-card.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EmployeeCardComponent {

  @Input()
  item: IEmployee;

  public getAvatar() {
    return this.item.photo ? this.item.photo.url : `https://www.gravatar.com/avatar/${md5(this.item.email)}?s=200`
  }
}
