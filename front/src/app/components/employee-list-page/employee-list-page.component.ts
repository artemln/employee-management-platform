import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {map, shareReplay} from 'rxjs/operators';
import {EmployeeStoreService, selectEmployeeItems} from '../../employee/employee-store.service';

@Component({
  selector: 'em-employee-list-page',
  templateUrl: './employee-list-page.component.html',
  styleUrls: ['./employee-list-page.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EmployeeListPageComponent implements OnInit, OnDestroy {

  itemsList$ = this.employeeService.store$.pipe(
    map(selectEmployeeItems()),
    shareReplay(1)
  );

  private listingSub: Subscription;

  constructor(
    private cd: ChangeDetectorRef,
    private employeeService: EmployeeStoreService
  ) {
  }

  ngOnInit() {
    this.listingSub = this.employeeService.loadListing()
      .subscribe(() => this.cd.detectChanges());
  }

  ngOnDestroy(): void {
    if (this.listingSub) {
      this.listingSub.unsubscribe();
    }
  }
}
