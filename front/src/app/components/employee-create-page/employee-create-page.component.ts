import {ChangeDetectionStrategy, Component, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {Observable, of} from 'rxjs';
import {EmployeeStoreService} from '../../employee/employee-store.service';
import {IEmployeeData} from '../../employee/employee.model';
import {EmployeeFormPagesComponents} from '../../guards/employee-form-can-deactivate.guard';
import {EmployeeFormComponent} from '../employee-form/employee-form.component';

@Component({
  selector: 'em-employee-create-page',
  templateUrl: './employee-create-page.component.html',
  styleUrls: ['./employee-create-page.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EmployeeCreatePageComponent implements OnInit, EmployeeFormPagesComponents {

  @ViewChild('employeeForm', { read: EmployeeFormComponent })
  private employeeForm: EmployeeFormComponent;

  // After item create complete.
  private isCreated = false;

  constructor(
    private router: Router,
    private employeeStore: EmployeeStoreService
  ) {
  }

  ngOnInit() {
  }

  public canDeactivate(): Observable<boolean> {
    const isEditing = !this.isCreated && this.employeeForm.isChanged;

    if (isEditing) {
      return of(window.confirm('Уйти без сохранения?'));
    } else {
      return of(true);
    }
  }

  public onSubmit(employee: IEmployeeData) {
    this.employeeStore.createItem(employee).subscribe(() => {
      this.isCreated = true;
      this.router.navigate(['/']); // Go to listing page.
    })
  }
}
