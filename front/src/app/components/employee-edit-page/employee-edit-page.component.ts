import {ChangeDetectionStrategy, Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable, of} from 'rxjs';
import {pluck, shareReplay} from 'rxjs/operators';
import {EmployeeStoreService} from '../../employee/employee-store.service';
import {IEmployee} from '../../employee/employee.model';
import {EmployeeFormComponent} from '../employee-form/employee-form.component';
import {EmployeeFormPagesComponents} from 'src/app/guards/employee-form-can-deactivate.guard';

@Component({
  selector: 'em-employee-edit-page',
  templateUrl: './employee-edit-page.component.html',
  styleUrls: ['./employee-edit-page.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EmployeeEditPageComponent implements OnInit, EmployeeFormPagesComponents {

  public employee$: Observable<IEmployee> = this.route.data.pipe(
    pluck<{employee: IEmployee}, IEmployee>('employee'),
    shareReplay(1)
  );

  @ViewChild('employeeForm', { read: EmployeeFormComponent })
  private employeeForm: EmployeeFormComponent;

  // After item update complete.
  private isUpdated = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private employeeStore: EmployeeStoreService
  ) {
  }

  ngOnInit() {
  }

  public canDeactivate(): Observable<boolean> {
    const isEditing = !this.isUpdated && this.employeeForm.isChanged;

    if (isEditing) {
      return of(window.confirm('Уйти без сохранения?'));
    } else {
      return of(true);
    }
  }

  public onChange(employee: IEmployee): void {
    this.employeeStore.updateItem(employee).subscribe(() => {
      // Pass canDeactivate.
      this.isUpdated = true;

      // Go to view page.
      // @todo add destination back URL.
      this.router.navigate(['employee', employee.id]);
    });
  }
}
