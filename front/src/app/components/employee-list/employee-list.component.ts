import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {IEmployee} from '../../employee/employee.model';

@Component({
  selector: 'em-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EmployeeListComponent implements OnInit {

  @Input()
  items: IEmployee[] = [];

  constructor() { }

  ngOnInit() {
  }

}
