import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import {AngularFireStorage, AngularFireUploadTask} from '@angular/fire/storage';
import {FormBuilder, Validators} from '@angular/forms';
import * as md5 from 'js-md5';
import * as uuid from 'uuid/v4';
import {EmployeeStoreService} from '../../employee/employee-store.service';
import {IEmployee, IEmployeeData} from '../../employee/employee.model';

interface HTMLFileChangeEvent extends Event {
  target: HTMLInputElement & EventTarget;
}

export interface IEmployeeForm {
  name: string;
  lastName: string;
  position: string;
  phone: string;
  email: string;
  birthday: string; // format - 2020-02-20
}

@Component({
  selector: 'em-employee-form',
  templateUrl: './employee-form.component.html',
  styleUrls: ['./employee-form.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EmployeeFormComponent implements OnInit, OnChanges, OnDestroy {

  @Input()
  public item?: IEmployee;

  public form = this.formBuilder.group({
    name: ['', Validators.required],
    lastName: ['', Validators.required],
    position: ['', Validators.required],
    phone: ['', Validators.required],
    email: ['', [Validators.required, Validators.email]],
    birthday: ['', Validators.required]
  });

  private isInitFormData = false;

  public get isChanged() {
    return this.form.dirty;
  }

  @Output()
  public onChange = new EventEmitter<IEmployee | IEmployeeData>();

  public uploadTask: AngularFireUploadTask;

  public isUploading = false;

  @ViewChild('fileInput', { read: ElementRef })
  fileInput: ElementRef<HTMLInputElement>;

  constructor(
    private cd: ChangeDetectorRef,
    private formBuilder: FormBuilder,
    private afStorage: AngularFireStorage,
    private employeeStore: EmployeeStoreService
  ) {
  }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!this.isInitFormData && changes.item) {
      // Only when init component.
      this.form.reset(this.modelToForm(this.item));

      this.isInitFormData = true;
    }
  }

  ngOnDestroy() {
    if (this.uploadTask) {
      this.uploadTask.cancel();
    }
  }

  public isNoAvatar(): boolean {
    return !this.item.photo;
  }

  public getAvatar(): string {
    return this.item.photo ? this.item.photo.url : `https://www.gravatar.com/avatar/${md5(this.item.email)}?s=160`
  }

  private getBirthdayDateTimestamp(birthday: string): number {
    const birthdayFullDate = new Date(birthday);
    const birthdayDate = new Date(0);

    birthdayDate.setMonth(
      birthdayFullDate.getUTCMonth(),
      birthdayFullDate.getUTCDate()
    );

    return Math.floor(birthdayDate.getTime() / 1000);
  }

  private modelToForm(employee: IEmployee): IEmployeeForm {
    return {
      ...employee,
      birthday: this.item.birthday.split('T').shift()
    }
  }

  private formToModel(employee: IEmployeeForm): IEmployee | IEmployeeData {
    const birthday = new Date(employee.birthday).toISOString();

    return {
      ...employee,
      id: this.item ? this.item.id : null, // When create.
      photo: this.item && this.item.photo ? this.item.photo : null,
      birthday,
      birthdayDateTimestamp: this.getBirthdayDateTimestamp(birthday)
    }
  }

  public onAvatarSubmit(event: HTMLFileChangeEvent) {
    this.isUploading = true;
    this.cd.markForCheck();

    const fileId = uuid();

    const ref = this.afStorage.ref(`/avatars/${fileId}`);

    this.uploadTask = ref.put(event.target.files[0]);

    this.uploadTask.then(
      () => {
        this.replaceAvatar(fileId)
          .then(item => {
            this.isUploading = false;
            this.item = item;
            this.cd.markForCheck();
          })
          .catch(() => {
            this.isUploading = false;
            this.cd.markForCheck();
          });

        this.fileInput.nativeElement.value = "";
        this.uploadTask = null;
        this.cd.markForCheck();
      },
      () => {
        this.fileInput.nativeElement.value = "";
        this.uploadTask = null;
        this.isUploading = false;
        this.cd.markForCheck();
      }
    );

    this.cd.markForCheck();
  }

  private replaceAvatar(id): Promise<IEmployee> {
    if (this.item.photo && this.item.photo.id) {
      return this.removeAvatar(this.item.photo.id).then(() => this.setAvatar(id));
    } else {
      return this.setAvatar(id);
    }
  }

  private setAvatar(id): Promise<IEmployee> {
    return new Promise(resolve => {
      this.afStorage.ref(`/avatars/${id}`)
        .getDownloadURL()
        .subscribe(url => {
          const item = {
            ...this.item,
            photo: {
              id,
              url
            }
          };

          this.employeeStore.updateItem(item).subscribe(newItem => resolve(newItem));
        });
    });


  }

  private removeAvatar(id): Promise<IEmployee> {
    return new Promise(resolve => {
      this.afStorage.ref(`/avatars/${id}`).delete().subscribe(() => {
        const item = {
          ...this.item,
          photo: null
        };

        this.employeeStore.updateItem(item).subscribe(newItem => resolve(newItem));
      });
    });
  }

  public onAvatarRemove() {
    if (this.item.photo && this.item.photo.id) {
      this.isUploading = true;
      this.cd.markForCheck();

      this.removeAvatar(this.item.photo.id)
        .then(item => {
          this.item = item;
          this.isUploading = false;
          this.cd.markForCheck();
        })
        .catch(() => {
          this.isUploading = false;
          this.cd.markForCheck();
        });
    }
  }

  public onSubmit() {
    if (this.form.valid) {
      this.onChange.next(this.formToModel(this.form.value));
    }
  }

  public onCancel() {
    this.form.reset(this.modelToForm(this.item));
  }
}
