import {ChangeDetectionStrategy, Component} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {pluck, shareReplay} from 'rxjs/operators';
import {EmployeeStoreService} from '../../employee/employee-store.service';
import {IEmployee} from '../../employee/employee.model';

@Component({
  selector: 'em-employee-page',
  templateUrl: './employee-page.component.html',
  styleUrls: ['./employee-page.component.less'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EmployeePageComponent {

  employee$: Observable<IEmployee> = this.route.data.pipe(
    pluck<{employee: IEmployee}, IEmployee>('employee'),
    shareReplay(1)
  );

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private employeeStore: EmployeeStoreService
  ) { }

  public onDeleteClick(e: MouseEvent, item) {
    e.preventDefault();

    if (window.confirm('Точно удалить?')) {
      this.employeeStore.removeItem(item.id).subscribe(() =>
        this.router.navigate(['/'])
      );
    }
  }
}
