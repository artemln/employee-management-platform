import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {IEmployee, IEmployeeData} from './employee.model';

@Injectable({
  providedIn: 'root'
})
export class EmployeeResourceService {

  private readonly endpoint = 'https://employee-management-platform.firebaseio.com/employees';

  constructor(
    private http: HttpClient
  ) {
  }

  private modelToDatabase(item: IEmployee): [IEmployee['id'], IEmployeeData] {
    const data = { ...item };
    const id = item.id;

    delete data.id;

    return [id, data];
  }

  private databaseToModel(data: IEmployeeData, id: IEmployee['id']): IEmployee {

    return {
      ...data,
      id
    } as IEmployee;
  }

  public get(id: string): Observable<IEmployee> {
    const headers = new HttpHeaders({'X-Firebase-Decoding': '1'});

    return this.http.get<IEmployeeData>(`${this.endpoint}/${id}.json`, { headers }).pipe(
      map((data) => this.databaseToModel(data, id)),
    );
  }

  public getAll(): Observable<IEmployee[]> {
    const headers = new HttpHeaders({'X-Firebase-Decoding': '1'});

    return this.http.get<{ [name: string]: IEmployeeData }>(`${this.endpoint}.json`, { headers }).pipe(
      map(list => Object.entries(list || {})
        .map(([id, data]) => this.databaseToModel(data, id))
      )
    );
  }

  public patch(item: IEmployee): Observable<IEmployee> {
    const headers = new HttpHeaders({'X-Firebase-Decoding': '1'});
    const [id, data] = this.modelToDatabase(item);

    return this.http.patch<IEmployeeData>(`${this.endpoint}/${id}.json`, data, { headers }).pipe(
      map((data) => this.databaseToModel(data, id))
    );
  }

  public post(data: IEmployeeData): Observable<IEmployee> {
    const headers = new HttpHeaders({'X-Firebase-Decoding': '1'});

    return this.http.post<{name: IEmployee['id']}>(`${this.endpoint}.json`, data, { headers }).pipe(
      map(({ name: id }) => this.databaseToModel(data, id))
    );
  }

  public delete(id: string): Observable<null> {
    const headers = new HttpHeaders({'X-Firebase-Decoding': '1'});

    return this.http.delete<null>(`${this.endpoint}/${id}.json`, { headers });
  }
}
