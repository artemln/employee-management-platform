
export interface IEmployee {
  id: string;
  name: string;
  lastName: string;
  position: string;
  phone: string;
  email: string;
  birthday: string;
  birthdayDateTimestamp: number;
  photo?: {
    id: string;
    url: string
  };
}

export interface IEmployeeData {
  name: string;
  lastName: string;
  position: string;
  phone: string;
  email: string;
  birthday: string;
  birthdayDateTimestamp: number;
  photo?: {
    id: string;
    url: string
  };
}
