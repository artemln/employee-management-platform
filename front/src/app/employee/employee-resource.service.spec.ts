import { TestBed } from '@angular/core/testing';

import { EmployeeResourceService } from './employee-resource.service';

describe('EmployeeResourceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EmployeeResourceService = TestBed.get(EmployeeResourceService);
    expect(service).toBeTruthy();
  });
});
