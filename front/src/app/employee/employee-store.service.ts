import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {map, switchMap, take, tap} from 'rxjs/operators';
import {EmployeeResourceService} from './employee-resource.service';
import {IEmployee, IEmployeeData} from './employee.model';

export interface IEmployeeStore {
  ids: IEmployee['id'][]
  items: {
    [key: string]: IEmployee
  }
}

export const selectEmployeeItems = () => ({ ids, items }: IEmployeeStore): IEmployee[] =>
  ids.map(id => items[id]);

export const selectEmployeeItem = (id: IEmployee['id']) => ({ items }: IEmployeeStore): IEmployee => items[id];

@Injectable({
  providedIn: 'root'
})
export class EmployeeStoreService {

  private store: BehaviorSubject<IEmployeeStore> = new BehaviorSubject<IEmployeeStore>({
    ids: [],
    items: {},
  });

  public store$: Observable<IEmployeeStore> = this.store.asObservable();

  constructor(
    private resource: EmployeeResourceService
  ) {
  }

  private upsertItems(items: IEmployee[]) {
    if (items.length) {
      let state = this.store.value;

      items.forEach(item => {
        const id = item.id;

        // Insert or update item.
        state = {
          ...state,
          items: {
            ...state.items,
            [item.id]: item
          }
        };

        // Insert id.
        if (!state.ids.includes(id)) {
          state = {
            ...state,
            ids: [...state.ids, id]
          };
        }
      });

      this.store.next(state);
    }
  }

  private removeItems(ids: IEmployee['id'][]) {
    if (ids.length) {
      let state = this.store.value;

      ids.forEach(id => {
        const ids = [ ...state.ids ];
        const items = { ...state.items };

        ids.splice(ids.indexOf(id), 1);
        delete items[id];

        state = {
          ids,
          items
        };
      });

      this.store.next(state);
    }
  }

  /**
   * Load items from server and save to store.
   */
  public loadListing(): Observable<IEmployee[]> {
    return this.resource.getAll().pipe(
      tap(items => this.upsertItems(items))
    );
  }

  /**
   * Load item from server and save to store.
   */
  public loadItem(id: IEmployee['id']): Observable<IEmployee> {
    return this.resource.get(id).pipe(
      tap(item => this.upsertItems([item]))
    );
  }

  /**
   * Load item from server and save to store.
   */
  public updateItem(item: IEmployee): Observable<IEmployee> {
    return this.resource.patch(item).pipe(
      tap(nextItem => this.upsertItems([nextItem]))
    );
  }

  /**
   * Load item from server and save to store.
   */
  public createItem(item: IEmployeeData): Observable<IEmployee> {
    return this.resource.post(item).pipe(
      tap(nextItem => this.upsertItems([nextItem]))
    );
  }

  /**
   * Load item from server and save to store.
   */
  public removeItem(id: IEmployee['id']): Observable<null> {
    return this.resource.delete(id).pipe(
      tap(() => this.removeItems([id]))
    );
  }

  /**
   * Get item from store or load from server.
   */
  public getItem(id: IEmployee['id']): Observable<IEmployee> {
    return this.store$.pipe(
      take(1),
      map(selectEmployeeItem(id)),
      switchMap(item => item
        ? of(item)
        : this.loadItem(id)
      ),
    )
  }
}
