import { TestBed, async, inject } from '@angular/core/testing';

import { EmployeeFormCanDeactivateGuard } from './employee-form-can-deactivate.guard';

describe('EmployeeFormCanDeactivateGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EmployeeFormCanDeactivateGuard]
    });
  });

  it('should ...', inject([EmployeeFormCanDeactivateGuard], (guard: EmployeeFormCanDeactivateGuard) => {
    expect(guard).toBeTruthy();
  }));
});
