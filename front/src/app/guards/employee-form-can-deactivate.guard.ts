import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanDeactivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';

export interface EmployeeFormPagesComponents {
  canDeactivate(): Observable<boolean>;
}

@Injectable({
  providedIn: 'root'
})
export class EmployeeFormCanDeactivateGuard implements CanDeactivate<EmployeeFormPagesComponents> {
  canDeactivate(
    component: EmployeeFormPagesComponents,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot
  ): Observable<boolean> {
    return component.canDeactivate();
  }
}
