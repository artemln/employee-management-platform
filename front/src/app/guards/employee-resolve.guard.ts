import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {EmployeeStoreService} from '../employee/employee-store.service';
import {IEmployee} from '../employee/employee.model';

@Injectable({
  providedIn: 'root'
})
export class EmployeeResolveGuard implements Resolve<IEmployee> {

  constructor(
    private employeeStore: EmployeeStoreService
  ) {
  }

  resolve(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<IEmployee> | Promise<IEmployee> | IEmployee {
    return this.employeeStore.getItem(next.params['id'] as string)
  }
}
