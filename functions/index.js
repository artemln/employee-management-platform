'use strict';

const functions = require('firebase-functions');
const admin = require('firebase-admin');
const mailController = require('@sendgrid/mail');

admin.initializeApp(functions.config().firebase);

mailController.setApiKey(functions.config().sendgrid.emailkey);

const sendBirthdayEmail = (email, name) => {
  const mailData = {
    to: email,
    from: `Employee management platform <larionov.artyom@gmail.com>`,
    subject: `С Днем Рождения ${name}!`,
    text: `${name}, мы поздравляем Вас с Днем Рождения!`,
  };

  return mailController.send(mailData);
};

exports.send_email_job = functions.pubsub
  .topic('birthday-email-hours-tick')
  .onPublish(() => {
    return admin.database().ref('/settings')
      .once('value')
      .then(snapshot => {
        const today = new Date();
        const timestampDate = new Date(0);
        const { beforeDays, sendTime } = snapshot.val();

        if (today.getUTCHours() !== parseInt(sendTime, 10)) {
          console.log('Not send. ', today.getUTCHours(), sendTime);

          return Promise.resolve();
        }

        timestampDate.setMonth(
          today.getUTCMonth(),
          today.getUTCDate() + parseInt(beforeDays, 10)
        );

        const timestamp = Math.floor(timestampDate.getTime() / 1000);

        console.log('Send. ', timestamp);

        // Select employees.
        return admin.database().ref('/employees')
          .orderByChild('birthdayDateTimestamp')
          .startAt(timestamp - 1)
          .endAt(timestamp + 1)
          .once('value')
          .then(snapshot => {
            const items = Object.values(snapshot.val() || {});

            console.log('Send Users. ', items);

            const emails = items
              .map(({email, name}) => sendBirthdayEmail(email, name));

            return Promise.all(emails);
          });
      });
  });
